﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float sensitivity;
    private Transform cam;
    private float xAxisClamp; // Limite de rotation pour l'awe x de notre caméra

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = this.gameObject.transform.GetChild(0).GetComponent<Transform>();
        xAxisClamp = 0.0f;
    }
    void Update()
    {
        playerMove();
        cameraRotate();
    }

    private void playerMove()
    {
        float moveX = speed * Input.GetAxis("Vertical");
        float moveY = speed * Input.GetAxis("Horizontal");

        transform.Translate(moveY, 0, moveX);
    }

    private void cameraRotate()
    {
        float cameraX = sensitivity * Input.GetAxis("Mouse X") * Time.deltaTime;
        float cameraY = sensitivity * Input.GetAxis("Mouse Y") * Time.deltaTime;

        xAxisClamp += cameraY;

        if (xAxisClamp > 90.0f)
        {
            xAxisClamp = 90.0f;
            cameraY = 0;
            clampXAxisRotation(270.0f);
        }
        else if (xAxisClamp < -90.0f)
        {
            xAxisClamp = -90.0f;
            cameraY = 0;
            clampXAxisRotation(90.0f);        
        }

        cam.Rotate(Vector3.left * cameraY);
        transform.Rotate(Vector3.up * cameraX);
    }

    private void clampXAxisRotation(float value)    // value une valeur en degres
    {
        Vector3 eulerRotation = cam.eulerAngles;  // On récupère l'angle dans un Vector3
        eulerRotation.x = value;                        // On injècte dans ce Vector3 notre value
        cam.eulerAngles = eulerRotation;          // On applique notre Vector3 a l'angle
    }
}
